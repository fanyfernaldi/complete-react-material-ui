import React from 'react';
import {
  AppBar,
  Toolbar,
  Grid,
  InputBase,
  IconButton,
  Badge,
  makeStyles,
} from '@material-ui/core';
import NotificationsNoneIcon from '@material-ui/icons/NotificationsNone';
import ChatBubbleOutlineIcon from '@material-ui/icons/ChatBubbleOutline';
import PowerSettingsNewIcon from '@material-ui/icons/PowerSettingsNew';
import SearchIcon from '@material-ui/icons/Search';

const useStyles = makeStyles((theme) => ({
  // saya memberi nama root karena ini akan dipakai di seluruh header
  root: {
    backgroundColor: '#fff',
    //biar ada bayangannya di bawahnya, jadi kayak stack
    // transform: 'translateZ(0)',  //karena udah make di App.js dan sasarannya sama maka ini dicomentar
  },
  searchInput: {
    opacity: '0,6',
    padding: `0px ${theme.spacing(1)}`,
    fontSize: '0.8rem',
    '&:hover': {
      backgroundColor: '#f2f2f2',
    },
    // memberi jarak kekanan icon search input (MuiSvgIcon-root) ke InputBase.
    // MuiSvgIcon-root tau dari mana? iconnya di inspect, keluarnya tulisan itu
    '& .MuiSvgIcon-root': {
      // theme.spacing(1) itu sama saja dengan 8 px, bisa lihat di docsnya material ui pada menu customization
      marginRight: theme.spacing(1),
    },
  },
}));

function Header() {
  const classes = useStyles();
  return (
    <AppBar position="static" className={classes.root}>
      <Toolbar>
        <Grid container alignItems="center">
          <Grid item>
            <InputBase
              placeholder="Search topics"
              className={classes.searchInput}
              startAdornment={<SearchIcon fontSize="small" />}
            />
          </Grid>
          <Grid item sm></Grid>
          <Grid item>
            <IconButton>
              <Badge badgeContent={4} color="secondary">
                <NotificationsNoneIcon fontSize="small" />
              </Badge>
            </IconButton>
            <IconButton>
              <Badge badgeContent={4} color="primary">
                <ChatBubbleOutlineIcon fontSize="small" />
              </Badge>
            </IconButton>
            <IconButton>
              <PowerSettingsNewIcon fontSize="small" />
            </IconButton>
          </Grid>
        </Grid>
      </Toolbar>
    </AppBar>
  );
}

export default Header;
