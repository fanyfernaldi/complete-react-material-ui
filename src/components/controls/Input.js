import React from 'react';
import { TextField } from '@material-ui/core';

export default function Input(props) {
  //error set to null as default value
  const { name, label, value, error = null, onChange, ...other } = props;
  return (
    <TextField
      variant="outlined"
      label={label}
      name={name}
      value={value}
      onChange={onChange}
      {...other}
      //untuk keperluan validasi, properti (yang merupakan key error:, helperText:) ini memang sudah ada di material-ui,
      //check this property error, if this property is not null then we will do the rest of the operations
      {...(error && { error: true, helperText: error })}
    />
  );
}
