import React from 'react';
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';

export default function DatePicker(props) {
  const { name, label, value, onChange } = props;

  //datepicker tidak menggunakan e.target.value, dia menggunakan date jadi kita convert dulu e.target.value ke date pilihannya
  const convertToDefEventPara = (name, value) => ({
    target: {
      //cara  detail
      // name: name
      // value: value,

      //cara lebih singkat
      name,
      value,
    },
  });

  return (
    <div>
      <MuiPickersUtilsProvider utils={DateFnsUtils}>
        <KeyboardDatePicker
          disableToolbar
          variant="inline"
          inputVariant="outlined"
          label={label}
          formate="MMM/dd/yyy"
          name={name}
          value={value}
          onChange={(date) => onChange(convertToDefEventPara(name, date))}
        />
      </MuiPickersUtilsProvider>
    </div>
  );
}
