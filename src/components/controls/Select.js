import React from 'react';
import {
  FormControl,
  InputLabel,
  Select as MuiSelect,
  MenuItem,
  FormHelperText,
} from '@material-ui/core';

export default function Select(props) {
  const { name, label, value, error = null, onChange, options } = props;

  return (
    //untuk select, berdasarkan dokumen mui, validasi error ditelakkan pada FormControl
    <FormControl variant="outlined" {...(error && { error: true })}>
      <InputLabel>{label}</InputLabel>
      {/* MuiSelect merupakan alias dari Select milik MuI(lihat di atas), tujuannya di aliaskan karena namanya sama dengan nama Component di file ini  */}
      <MuiSelect label={label} name={name} value={value} onChange={onChange}>
        {/* ini optionnya  */}
        <MenuItem value="">None</MenuItem>
        {options.map((item) => (
          <MenuItem key={item.id} value={item.id}>
            {item.title}
          </MenuItem>
        ))}
      </MuiSelect>
      {/* untuk select, helperTexnya (untuk nampilin tulisan error) ada disini  */}
      {error && <FormHelperText>{error}</FormHelperText>}
    </FormControl>
  );
}
