import React from 'react';
import {
  FormControl,
  FormControlLabel,
  Checkbox as MuiCheckBox,
} from '@material-ui/core';

export default function Chechbox(props) {
  const { name, label, value, onChange } = props;

  //checkbox tidak menggunakan .value property, dia menggunakan .checked property jadi kita convert dulu e.target.value ke e.target.checked
  const convertToDefEventPara = (name, value) => ({
    target: {
      //cara  detail
      // name: name
      // value: value,

      //cara lebih singkat
      name,
      value,
    },
  });

  return (
    <FormControl>
      <FormControlLabel
        control={
          <MuiCheckBox
            name={name}
            color="primary"
            checked={value}
            onChange={(e) =>
              onChange(convertToDefEventPara(name, e.target.checked))
            }
          />
        }
        label={label}
      />
    </FormControl>
  );
}
