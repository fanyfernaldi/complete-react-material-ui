import React from 'react';
import { Button as MuiButton, makeStyles } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  root: {
    margin: theme.spacing(0.5), //0.5 berarti 4px
  },
  label: {
    textTransform: 'none',
  },
}));

export default function Button(props) {
  //jika ada property selain yang di destructivekan, kita bisa access dengan property ...other
  //contoh penggunaan ...other yaitu di button pada EmployeeForm.js saya memberi property type="submit" dan color="default",
  //padahal property type dan property color tidak di definisikan disini,
  const { text, size, color, variant, onClick, ...other } = props;
  const classes = useStyles();

  return (
    //itu di variant, maksudnya jika variantnya tidak di set maka defaultnya variant="contained". Analogikan ke yang lain
    <MuiButton
      variant={variant || 'contained'}
      size={size || 'large'}
      color={color || 'primary'}
      onClick={onClick}
      {...other}
      //propery classes ini ada dari Material-ui untuk styling, key dari objek didalam classes didapat dari css rulename untuk button, ada di API Button
      classes={{ root: classes.root, label: classes.label }}
    >
      {text}
    </MuiButton>
  );
}
