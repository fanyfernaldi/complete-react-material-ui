import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core';

//untuk keperluan realtime validation, kita tambah 2 parameter yaitu validateOnChange dan validate
export function useForm(initialFValues, validateOnChange = false, validate) {
  const [values, setValues] = useState(initialFValues);
  const [errors, setErrors] = useState({});

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setValues({
      ...values,
      //name yang ada di input TextField diisi dengan value yang kita ketik di TextField
      [name]: value,
    });
    //check whether this form asks for this feature for single control validatoin, so we can check that with this property validateOnChange
    if (validateOnChange)
      //into this function we are going to pass an object. Inside this object will be having a single property name, it will be replaced by
      //control name property from which onChange even is triggered
      validate({ [name]: value });
  };

  const resetForm = () => {
    setValues(initialFValues);
    setErrors({});
  };

  // return an object containing reusable variables and functions
  return { values, errors, setErrors, setValues, handleInputChange, resetForm };
}

const useStyles = makeStyles((theme) => ({
  root: {
    '& .MuiFormControl-root': {
      width: '80%',
      margin: theme.spacing(1),
    },
  },
}));

export function Form(props) {
  const classes = useStyles();
  //salah satu (keknya memang cuma satu) penerapan dari ...others ada di employeeForm.js pada property onSubmit
  const { children, ...other } = props;
  return (
    <form className={classes.root} autoComplete="off" {...other}>
      {props.children}
    </form>
  );
}
