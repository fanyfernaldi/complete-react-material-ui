import React from 'react';
import { makeStyles, withStyles } from '@material-ui/core';

//withStyles sama makeStyles fungsinya sama aja (untuk memembuat kodingan css di dalam file js), cuma cara penulisannya aja yang beda.

//di kodingan ini styling cssnya make yang withStyles
const style = {
  sideMenu: {
    dislpay: 'flex',
    flexDirection: 'column',
    position: 'absolute',
    left: '0px',
    width: '320px',
    height: '100%',
    backgroundColor: '#253053',
  },
};

const SideMenu = (props) => {
  const { classes } = props;
  return <div className={classes.sideMenu}></div>;
};

export default withStyles(style)(SideMenu);
