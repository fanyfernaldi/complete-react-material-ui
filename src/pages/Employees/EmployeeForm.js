import React, { useEffect } from 'react';
import { Grid } from '@material-ui/core';
import Controls from '../../components/controls/Controls';
import { useForm, Form } from '../../components/useForm';
import * as employeeService from '../../services/employeeService';

//create an array containing gender item, untuk kepentingan radio button
const genderItems = [
  { id: 'male', title: 'Male' },
  { id: 'female', title: 'Female' },
  { id: 'other', title: 'Other' },
];

// key yang ada di initial values ini harus menjadi acuan nama/name dari tiap2 input field
const initialFValues = {
  id: 0,
  fullName: '',
  email: '',
  mobile: '',
  city: '',
  gender: 'male', //select dropdown
  departmentId: '', //select dropdown
  hireDate: new Date(),
  isPermanent: false, //checkbox
};

export default function EmployeeForm(props) {
  const { addOrEdit, recordForEdit } = props;
  //function for validation
  //fielcValues by default, it will be having the values object
  const validate = (fieldValues = values) => {
    //inside this temp object, we will be saving the validation error message
    //in that case if we assign this temp object with an empry object, it will clear existing error inside the form in order to avoid
    //that, we will save the existing error inside this temp here, errors, and based on the property that we have passed,
    //it will only update thatproperty for the error message
    let temp = { ...errors };
    // we add this if statement for each validation , we will check whether this property fullName is inside this fieldValues here
    // so this is required for single control validation
    if ('fullName' in fieldValues)
      //check the value of fullName property inside the state object values
      temp.fullName = fieldValues.fullName ? '' : 'this field is required';
    if ('email' in fieldValues)
      //it is not mandatory but if there ism any value inside the checkbox here, it should be in proper(harus tepat) email format
      //we use a simple regular expression for email. If true email will valid, if false, email not valid
      temp.email = /$^|.*@.*..*/.test(fieldValues.email)
        ? ''
        : 'Email is not valid';
    if ('mobile' in fieldValues)
      temp.mobile =
        fieldValues.mobile.length > 9 ? '' : 'Minimum 10 numbers required';
    if ('departmentId' in fieldValues)
      temp.departmentId =
        fieldValues.departmentId.length !== 0 ? '' : 'this field is required';
    //save the errors
    setErrors({
      ...temp,
    });

    // check whether fieldValues is equal to values or not
    if (fieldValues === values)
      //this .values function return a collection of values inside temp object which are email, fulName, mobile, and departmentId.
      //This .every method test whether all elements in the array pass the test implemented by provided function, it returns a Boolean value.
      //Sooo..if all of the propertyies inside this temp object is an empty string then we could say that this form as whole is valid.
      //If this condition true for all the of the properties inside the object, then it will return true, otherwise it will return false
      return Object.values(temp).every((x) => x === '');
  };

  //useForm ini merupakan custom hook buatan saya, tujuannya agar bisa di reusable
  //di dalam useForm, dia mereturn ke values, setValues, dan handleInputChange
  const {
    values,
    setValues,
    errors,
    setErrors,
    handleInputChange,
    resetForm,
  } = useForm(initialFValues, true, validate);

  const handleSubmit = (e) => {
    e.preventDefault();
    //this validate function must return a boolean value indicating whether the form is valid or not
    if (validate()) addOrEdit(values, resetForm);
  };

  //whenever we change the value of recordForEdit, this callback function will be invoke
  useEffect(() => {
    //fyi, recordForEdit ini untuk mengisi object dari emplyee yg kita klik untuk di edit
    if (recordForEdit != null)
      setValues({
        //with the spread operator here, property from recordForEdit can be apart of recordForEdit
        //and the same will be updated into values object
        ...recordForEdit,
      });
  }, [recordForEdit]);

  return (
    // <Form> ini merupakan component buatan saya yang sudah diberi styling, tujuannya agar bisa di reusable
    <Form onSubmit={handleSubmit}>
      <Grid container>
        <Grid item xs={6}>
          {/* input yang digunakan disini bukan menggunakan material-ui, namun menggunakan Input buatan saya, ini untuk reusable saja  */}
          <Controls.Input
            name="fullName"
            label="Full Name"
            value={values.fullName}
            onChange={handleInputChange}
            error={errors.fullName}
          />
          <Controls.Input
            name="email"
            label="Email"
            value={values.email}
            onChange={handleInputChange}
            error={errors.email}
          />
          <Controls.Input
            name="mobile"
            label="Mobile"
            value={values.mobile}
            onChange={handleInputChange}
            error={errors.mobile}
          />
          <Controls.Input
            name="city"
            label="City"
            value={values.city}
            onChange={handleInputChange}
          />
        </Grid>
        <Grid item xs={6}>
          <Controls.RadioGroup
            name="gender"
            label="Gender"
            value={values.gender}
            onChange={handleInputChange}
            items={genderItems}
          />
          <Controls.Select
            name="departmentId"
            label="Department"
            value={values.departmentId}
            onChange={handleInputChange}
            options={employeeService.getDepartmentCollection()}
            error={errors.departmentId}
          />
          <Controls.DatePicker
            name="hireDate"
            label="Hire Date"
            value={values.hireDate}
            onChange={handleInputChange}
          />
          <Controls.Checkbox
            name="isPermanent"
            label="Permanent Employee"
            value={values.isPermanent}
            onChange={handleInputChange}
          />
          <div>
            <Controls.Button type="submit" text="Submit" />
            <Controls.Button color="default" text="Reset" onClick={resetForm} />
          </div>
        </Grid>
      </Grid>
    </Form>
  );
}
