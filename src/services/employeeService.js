// for avoiding the mistake of typing this string
const KEYS = {
  employees: 'employees',
  employeeId: 'employeeId',
};

//in an practical application there will be a back-end API from which we can fetch all of the department inside the application.
//In this project we not using API but using a local storage

//Inside this service file here we'll be adding multiple function to fetch data from the backend (with API) or data storage.
//In this project we using local data storage
export const getDepartmentCollection = () => [
  { id: '1', title: 'Development' },
  { id: '2', title: 'Marketing' },
  { id: '3', title: 'Accounting' },
  { id: '4', title: 'HR' },
];

//we will insert new employee and store it using browser local storage instead of some backends API
//employee data are saved inside local storage as an array
export function insertEmployee(data) {
  //retrieve all employees
  let employees = getAllEmployees();
  console.log('employees', employees);
  data['id'] = genereateEmployeeId();
  employees.push(data);
  //.setItem is used for inserting that particular object. The first parameter is keys, second parameter is value of key
  //before we store to local storage, we should convert into json string
  localStorage.setItem(KEYS.employees, JSON.stringify(employees));
}

export function updateEmployee(data) {
  //retrieve all of the employees
  let employees = getAllEmployees();
  //find the recordIndex from this employees array with data property
  let recordIndex = employees.findIndex((x) => x.id === data.id);
  //update that corresponding employees record with the data that we have pass to this function
  employees[recordIndex] = { ...data };
  //update the local storage for array of employees here
  localStorage.setItem(KEYS.employees, JSON.stringify(employees));
}

export function deleteEmployee(id) {
  //retrieve all of the employees
  let employees = getAllEmployees();
  //filter all of the employees except the id that we have passed here
  employees = employees.filter((x) => x.id !== id);
  //update the local storage for array of employees
  localStorage.setItem(KEYS.employees, JSON.stringify(employees));
}

//function for generate employee id
export function genereateEmployeeId() {
  if (localStorage.getItem(KEYS.employeeId) == null)
    localStorage.setItem(KEYS.employeeId, '0');
  var id = parseInt(localStorage.getItem(KEYS.employeeId));
  localStorage.setItem(KEYS.employeeId, (++id).toString());
  return id;
}

//defining the function for retrieving(mengambil) all the employees records that we have inside the KEY employees
export function getAllEmployees() {
  //chect whether(apakah) that particular(tertentu) local storage item is already occupied(terisi) or not, if it is null we will
  //insert an empty array into that particular key and finally from this function will return the array of employees
  if (localStorage.getItem(KEYS.employees) == null)
    localStorage.setItem(KEYS.employees, JSON.stringify([]));

  //Kalo ngga ada select dropdown kit sampe sini aja, let employeesnya diganti jadi return
  let employees = JSON.parse(localStorage.getItem(KEYS.employees));

  //map departmentID to department title. Kenapa di map? karena pada saat simpan ke storage kita nyimpannya idnya, jd perlu di convert ke titlemnya
  let departments = getDepartmentCollection();
  return employees.map((x) => ({
    ...x,
    department: departments[x.departmentId - 1].title,
  }));
}
