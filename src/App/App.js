import React from 'react';
import './App.css';
import SideMenu from '../components/SideMenu';
import {
  makeStyles,
  CssBaseline,
  createMuiTheme,
  ThemeProvider,
} from '@material-ui/core';
import Header from '../components/Header';
import Employees from '../pages/Employees/Employees';

//customization theme milik Material ui,
//palette, shape, secondary, dkk tahunya dari docs material ui -> customization -> defaul theme, kita override css-css bawaannya
const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#333996',
      light: '#3c44b126',
    },
    secondary: {
      main: '#f83245',
      light: '#f8324526',
    },
    background: {
      default: '#f4f5fd',
    },
  },
  shape: {
    borderRadius: '4px',
  },
  //you can also override the materialUI component globally inside the application
  overrides: {
    //mengoverride component AppBar milik Material UI
    //MuiAppBar merupakan nama dari component AppBar. Tahu dari mana? search AppBar API di mui docs, disitu ada tulisan Component Name nya
    MuiAppBar: {
      // root tau dari mana? di bagian css pada AppBar API pada mui docs
      root: {
        transform: 'translateZ(0)',
      },
    },
  },
  props: {
    //MuiIconButton merupakan nama dari component IconButton. Tahu dari mana? search IconButton API di mui docs, disitu ada tulisan Component Name nya
    MuiIconButton: {
      //if true the riple effect will be disabled
      disableRipple: true,
    },
  },
});

//jika di SideMenu.js kita pake cara withStyle, disini kita pake makeStyles untuk styling cssnya
const useStyles = makeStyles({
  appMain: {
    paddingLeft: '320px',
    width: '100%',
  },
});

function App() {
  const classes = useStyles();

  return (
    <ThemeProvider theme={theme}>
      <SideMenu />
      <div className={classes.appMain}>
        <Header />
        <Employees />
      </div>
      {/* karena di atas kita mendefinisikan paddingLeft 320px, maka kita harus make CSSBaseLine, 
      jika ditak pake CSSBaseLine maka dampaknya tampilan web bisa di scroll ke kanan sebanyak 320px*/}
      <CssBaseline />
    </ThemeProvider>
  );
}

export default App;
